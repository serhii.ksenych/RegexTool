﻿using Prism.Commands;
using Prism.Mvvm;
using RegexTool.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RegexTool.ViewModels
{
    public class ShellViewModel : BindableBase
    {
        #region Fields and properties

        private IRegexService _regexService;

        private string _pattern;
        public string Pattern { get { return _pattern; } set { _pattern = value; RaisePropertyChanged(nameof(Pattern)); } }

        private string _inputText;
        public string InputText { get { return _inputText; } set { _inputText = value; RaisePropertyChanged(nameof(InputText)); } }

        private string _resultText;
        public string ResultText { get { return _resultText; } set { _resultText = value; RaisePropertyChanged(nameof(ResultText)); } }

        private bool _isMultiline;
        public bool IsMultiline { get { return _isMultiline; } set { _isMultiline = value; RaisePropertyChanged(nameof(IsMultiline)); } }

        private bool _isMultipleMatch;
        public bool IsMultipleMatch { get { return _isMultipleMatch; } set { _isMultipleMatch = value; RaisePropertyChanged(nameof(IsMultipleMatch)); } }

        #endregion

        #region Commands

        public ICommand CommandRegexIt => new DelegateCommand(()=> 
        {
            try
            {
                Task task = Task.Run(() =>
                {
                    ResultText = _regexService.RegexIt(_inputText, _pattern, _isMultiline, _isMultipleMatch);
                });
            }

            catch (Exception ex)
            {
                ResultText = ex.Message;
            }
        });

        #endregion

        #region Ctor

        public ShellViewModel(IRegexService regexService)
        {
            _regexService = regexService;
        }

        #endregion
    }
}
