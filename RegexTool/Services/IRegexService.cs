﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegexTool.Services
{
    public interface IRegexService
    {
        string RegexIt(string inputText, string pattern, bool isMultiline, bool isMultipleMatch);
    }
}
